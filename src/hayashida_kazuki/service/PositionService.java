package hayashida_kazuki.service;

import static hayashida_kazuki.utils.CloseableUtil.*;
import static hayashida_kazuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import hayashida_kazuki.beans.Position;
import hayashida_kazuki.dao.PositionDao;

public class PositionService {

	public List<Position> getPositions() {

		Connection connection = null;
		try {
			connection = getConnection();

			PositionDao positionDao = new PositionDao();
			List<Position> ret = positionDao.getPositions(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
