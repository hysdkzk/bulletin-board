package hayashida_kazuki.service;

import static hayashida_kazuki.utils.CloseableUtil.*;
import static hayashida_kazuki.utils.DBUtil.*;

import java.sql.Connection;

import hayashida_kazuki.beans.User;
import hayashida_kazuki.dao.UserDao;
import hayashida_kazuki.utils.CipherUtil;

public class LoginService {

	public User login(String id, String password) {

		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);
			User user = userDao.getLoginUser(connection, id, encPassword);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
