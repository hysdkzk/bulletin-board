package hayashida_kazuki.service;

import static hayashida_kazuki.utils.CloseableUtil.*;
import static hayashida_kazuki.utils.DBUtil.*;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import hayashida_kazuki.beans.Post;
import hayashida_kazuki.dao.PostDao;

public class PostService {

	public void newPost(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public List<Post> getPosts(String word, String start, String end) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			List<Post> ret = postDao.getPosts(connection, word, start, end);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}


	public void deletePost(int postId) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.delete(connection, postId);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Date getPostMinDate() {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			Date ret = postDao.getPostMinDate(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Date getPostMaxDate() {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			Date ret = postDao.getPostMaxDate(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public boolean isPostExist() {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			boolean ret = postDao.isPostExist(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
