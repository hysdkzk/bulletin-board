package hayashida_kazuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayashida_kazuki.beans.Post;
import hayashida_kazuki.beans.User;
import hayashida_kazuki.service.PostService;

@WebServlet(urlPatterns = { "/new_post" })
public class NewPostServlet extends HttpServlet {
	private static  final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("/new_post.jsp").forward(request,  response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<>();

		HttpSession session = request.getSession();
		if ( isValid(request, messages) ) {

			User loginUser = (User) session.getAttribute("loginUser");

			Post post = new Post();
			post.setSubject(request.getParameter("subject"));
			post.setText(request.getParameter("text"));
			post.setCategory(request.getParameter("category"));
			post.setUserId(loginUser.getId());

			new PostService().newPost(post);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages",  messages);
			response.sendRedirect("new_post");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String subject = request.getParameter("subject");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (subject.isEmpty() || subject.length() > 30 ) {
			messages.add("件名は30文字以下で入力してください");
		}
		if (text.isEmpty() || text.length() > 1000) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if (category.isEmpty() || category.length() > 10) {
			messages.add("カテゴリーは10文字以下で入力してください");
		}
		if (messages.size() ==0) {
			return true;
		} else {
			return false;
		}
	}
}
