package hayashida_kazuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayashida_kazuki.beans.Branch;
import hayashida_kazuki.beans.Position;
import hayashida_kazuki.beans.User;
import hayashida_kazuki.service.BranchService;
import hayashida_kazuki.service.PositionService;
import hayashida_kazuki.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static  final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		BranchService branchService = new BranchService();
		List<Branch> branches = branchService.getBranches();
		PositionService positionService = new PositionService();
		List<Position> positions = positionService.getPositions();

		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("/signup.jsp").forward(request,  response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<>();

		if ( isValid(request, messages) ) {
			User user = new User();
			user.setId(request.getParameter("id"));
			user.setPassword(request.getParameter("password"));
			user.setUserName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch")));
			user.setPositionId(Integer.parseInt(request.getParameter("position")));

			new UserService().register(user);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages",  messages);
			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));

		if (!id.matches("[A-Za-z0-9]{6,20}")) {
			messages.add("アカウント名は半角英数字で6文字以上20文字以下で入力してください");
		}
		if (!password.matches("[-_@+*;:#$%&A-Za-z0-9]{6,20}")) {
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
		}
		if (name.isEmpty() || name.length() > 10) {
			messages.add("名称は10文字以下で入力してください");
		}
		if (!password.equals(password2)) {
			messages.add("パスワードが合っていません");
		}
		if (branch != 1 && (position == 1 || position == 2)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}
		if (new UserService().isIdExist(id)) {
			messages.add("IDが既に利用されています");
		}
		if (messages.size() ==0) {
			return true;
		} else {
			return false;
		}
	}
}
