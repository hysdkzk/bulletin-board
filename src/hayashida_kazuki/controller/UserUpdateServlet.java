package hayashida_kazuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayashida_kazuki.beans.Branch;
import hayashida_kazuki.beans.Position;
import hayashida_kazuki.beans.User;
import hayashida_kazuki.service.BranchService;
import hayashida_kazuki.service.PositionService;
import hayashida_kazuki.service.UserService;

@WebServlet(urlPatterns = { "/user_update" })
public class UserUpdateServlet extends HttpServlet {
	private static  final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		response.sendRedirect("./");
	}


	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<>();

		HttpSession session = request.getSession();
		if ( isValid(request, messages) ) {
			String editUserId = request.getParameter("edit_user_id");
			User user = new User();
			user.setId(request.getParameter("id"));
			user.setUserName(request.getParameter("name"));
			user.setBranchId(Integer.parseInt(request.getParameter("branch")));
			user.setPositionId(Integer.parseInt(request.getParameter("position")));
			if (request.getParameter("password") != "") {
				user.setPassword(request.getParameter("password"));
			}

			new UserService().update(user, editUserId);

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages",  messages);

			BranchService branchService = new BranchService();
			List<Branch> branches = branchService.getBranches();
			PositionService positionService = new PositionService();
			List<Position> positions = positionService.getPositions();

			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("user_id", request.getParameter("user_id"));
			request.setAttribute("user_name", request.getParameter("user_name"));
			request.setAttribute("branch_id", request.getParameter("branch_id"));
			request.setAttribute("position_id", request.getParameter("position_id"));

			request.getRequestDispatcher("user_edit").forward(request,  response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));

		if (!id.matches("[A-Za-z0-9]{6,20}")) {
			messages.add("アカウント名は半角英数字で6文字以上20文字以下で入力してください");
		}
		if (password.matches("[-_@+*;:#$%&A-Za-z0-9]{1,5}")) {
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
		}
		if (name.isEmpty() || name.length() >= 10) {
			messages.add("名称は10文字以下で入力してください");
		}
		if (!password.equals(password2)) {
			messages.add("パスワードが合っていません");
		}
		if (branch != 1 && (position == 1 || position == 2)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}
		if (new UserService().isIdExist(id)) {
			messages.add("IDが既に利用されています");
		}
		if (messages.size() ==0) {
			return true;
		} else {
			return false;
		}
	}
}
