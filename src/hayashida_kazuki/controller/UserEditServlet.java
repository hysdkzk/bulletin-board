package hayashida_kazuki.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hayashida_kazuki.beans.Branch;
import hayashida_kazuki.beans.Position;
import hayashida_kazuki.beans.User;
import hayashida_kazuki.service.BranchService;
import hayashida_kazuki.service.PositionService;
import hayashida_kazuki.service.UserService;

@WebServlet(urlPatterns = { "/user_edit" })
public class UserEditServlet extends HttpServlet {
	private static  final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		response.sendRedirect("./");
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		BranchService branchService = new BranchService();
		List<Branch> branches = branchService.getBranches();
		PositionService positionService = new PositionService();
		List<Position> positions = positionService.getPositions();
		UserService userService = new UserService();
		User editUser = userService.getUser(request.getParameter("edit_user_id"));

		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.setAttribute("editUser", editUser);

		request.getRequestDispatcher("/user_edit.jsp").forward(request,  response);
	}
}
