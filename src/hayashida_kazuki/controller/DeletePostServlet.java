package hayashida_kazuki.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hayashida_kazuki.service.PostService;

@WebServlet("/delete_post")
public class DeletePostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		response.sendRedirect("./");
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int postId;
		postId = Integer.parseInt(request.getParameter("post_id"));

		new PostService().deletePost(postId);

		response.sendRedirect("./");
	}

}
