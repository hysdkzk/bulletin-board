package hayashida_kazuki.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayashida_kazuki.beans.Post;
import hayashida_kazuki.beans.User;
import hayashida_kazuki.service.PostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet {
	private static  final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		if (new PostService().isPostExist()) {
			request = filter(request, response);
		}

		request.getRequestDispatcher("/home.jsp").forward(request,  response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		User loginUser = new User();
		loginUser = (User)session.getAttribute("loginUser");

		if (loginUser == null) {

			response.sendRedirect("login");
		} else {

			request = filter(request, response);

			request.getRequestDispatcher("/home.jsp").forward(request,  response);
		}
	}


	private HttpServletRequest filter (HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		String category = (request.getParameter("category_filter") == null) ?
				"" : request.getParameter("category_filter");

		SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date minDate = new PostService().getPostMinDate();
		Date maxDate = new PostService().getPostMaxDate();

		String start = "";
		if (request.getParameter("start") != null) {
			if(request.getParameter("start").matches("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}")) {
				start = request.getParameter("start");
			} else {
				start = sdFormat.format(minDate);
			}
		} else {
			start = sdFormat.format(minDate);
		}

		String end = "";
		if (request.getParameter("end") != null) {
			if(request.getParameter("end").matches("[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}")) {
				end = request.getParameter("end");
			} else {
				end = sdFormat.format(maxDate);
			}
		} else {
			end = sdFormat.format(maxDate);
		}

		List<Post> posts = new ArrayList<>();
		try {
			Calendar calendar = Calendar.getInstance();
			Date endDate = sdFormat.parse(end);
			calendar.setTime(endDate);
			calendar.add(Calendar.DATE,  1);
			endDate = calendar.getTime();
			posts = new PostService().getPosts(category, start, sdFormat.format(endDate));
		} catch (ParseException e) {
			List<String> messages = new ArrayList<>();
			messages.add("エラーが起きました");
			session.setAttribute("errorMessages", messages);
		}

		request.setAttribute("posts",  posts);
		request.setAttribute("category",  category);
		request.setAttribute("start", start);
		request.setAttribute("end", end);

		return request;
	}


}
