package hayashida_kazuki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hayashida_kazuki.beans.User;
import hayashida_kazuki.service.LoginService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("/login.jsp").forward(request,  response);
	}

	@Override
    protected void doPost(HttpServletRequest request,HttpServletResponse response)
    		throws IOException, ServletException {

		String id = request.getParameter("id");
		String password = request.getParameter("password");
		List<String> messages = new ArrayList<>();

		LoginService loginService = new LoginService();
		User loginUser = loginService.login(id, password);

		HttpSession session = request.getSession();
		if (loginUser != null) {

			if (loginUser.getIsStop() == 1) {

				messages.add("アカウント停止中のためログインできません");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("login");
			} else {

				session.setAttribute("loginUser",  loginUser);
				response.sendRedirect("./");
			}
		} else {

			messages = new ArrayList<>();
			messages.add("ログインに失敗しました。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("login");
		}
	}

}
