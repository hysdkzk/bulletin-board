package hayashida_kazuki.dao;

import static hayashida_kazuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import hayashida_kazuki.beans.User;
import hayashida_kazuki.exception.NoRowsUpdatedRuntimeException;
import hayashida_kazuki.exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users (");
			sql.append(" id");
			sql.append(", password");
			sql.append(", user_name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_stop");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", 0");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1,  user.getId());
			ps.setString(2,  user.getPassword());
			ps.setString(3,  user.getUserName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getPositionId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user, String editUserId) {

		PreparedStatement ps1 = null;
		PreparedStatement ps2 = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" id = ?");
			sql.append(", user_name = ?");
			sql.append(", branch_id = ?");
			sql.append(", position_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps1 = connection.prepareStatement(sql.toString());

			ps1.setString(1,  user.getId());
			ps1.setString(2,  user.getUserName());
			ps1.setInt(3, user.getBranchId());
			ps1.setInt(4, user.getPositionId());
			ps1.setString(5,  editUserId);

			int count = ps1.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}

			if (user.getPassword() != null) {
				sql = new StringBuilder();
				sql.append("UPDATE users SET");
				sql.append(", password = ?");
				sql.append(" WHERE");
				sql.append(" id = ?");

				ps2 = connection.prepareStatement(sql.toString());

				ps2.setString(1,  user.getPassword());
				ps2.setString(2,  user.getId());

				count = ps2.executeUpdate();
				if (count == 0) {
					throw new NoRowsUpdatedRuntimeException();
				}
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps1);
			close(ps2);
		}
	}

	public void updateIsStop(Connection connection, String userId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append(" is_stop = if(is_stop=1,0,1)");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1,  userId);

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getLoginUser(Connection connection, String id, String password) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.*");
			sql.append(" , branches.branch_name");
			sql.append(" , positions.position_name");
			sql.append(" FROM users");
			sql.append(" INNER JOIN branches");
			sql.append(" ON users.branch_id = branches.id");
			sql.append(" INNER JOIN positions");
			sql.append(" ON users.position_id = positions.id");
			sql.append(" WHERE users.id = ? AND users.password = ?");


			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, id);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if ( userList.isEmpty() == true) {
				return null;
			} else if ( userList.size() >= 2) {
				throw new IllegalStateException("userList.size() >= 2");
			} else {
				return userList.get(0);
			}

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<>();
		try {
			while (rs.next()) {
				String id = rs.getString("id");
				String password = rs.getString("password");
				String userName = rs.getString("user_name");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isStop = rs.getInt("is_stop");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				String branchName = rs.getString("branch_name");
				String positionName = rs.getString("position_name");

				User user = new User();
				user.setId(id);
				user.setPassword(password);
				user.setUserName(userName);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setIsStop(isStop);
				user.setCreatedDate(createdDate);
				user.setUpdatedDate(updatedDate);
				user.setBranchName(branchName);
				user.setPositionName(positionName);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<User> getUsers(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.*");
			sql.append(" , branches.branch_name");
			sql.append(" , positions.position_name");
			sql.append(" FROM users");
			sql.append(" INNER JOIN branches");
			sql.append(" ON users.branch_id = branches.id");
			sql.append(" INNER JOIN positions");
			sql.append(" ON users.position_id = positions.id");
			sql.append(" ORDER BY created_date ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.*");
			sql.append(" , branches.branch_name");
			sql.append(" , positions.position_name");
			sql.append(" FROM users");
			sql.append(" INNER JOIN branches");
			sql.append(" ON users.branch_id = branches.id");
			sql.append(" INNER JOIN positions");
			sql.append(" ON users.position_id = positions.id");
			sql.append(" WHERE users.id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			return ret.get(0);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public boolean isIdExist (Connection connection, String id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.*");
			sql.append(" , branches.branch_name");
			sql.append(" , positions.position_name");
			sql.append(" FROM users");
			sql.append(" INNER JOIN branches");
			sql.append(" ON users.branch_id = branches.id");
			sql.append(" INNER JOIN positions");
			sql.append(" ON users.position_id = positions.id");
			sql.append(" WHERE users.id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUserList(rs);
			if (ret.size() == 0) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


}
