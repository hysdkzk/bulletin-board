package hayashida_kazuki.dao;

import static hayashida_kazuki.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import hayashida_kazuki.beans.Comment;
import hayashida_kazuki.beans.Post;
import hayashida_kazuki.exception.SQLRuntimeException;

public class PostDao {

	public void insert(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts ( ");
			sql.append("subject");
			sql.append(", text");
			sql.append(", category");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(", user_id");
			sql.append(", is_deleted");
			sql.append(") VALUES (");
			sql.append("?");
			sql.append(", ?");
			sql.append(", ?");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", CURRENT_TIMESTAMP");
			sql.append(", ?");
			sql.append(", 0");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1,  post.getSubject());
			ps.setString(2,  post.getText());
			ps.setString(3,  post.getCategory());
			ps.setString(4, post.getUserId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


	private List<Post> toPostList(Connection connection, ResultSet rs) throws SQLException {

		List<Post> ret = new ArrayList<>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String text = rs.getString("text");
				String category = rs.getString("category");
				Timestamp createdDate = rs.getTimestamp("created_date");
				Timestamp updatedDate = rs.getTimestamp("updated_date");
				String userId = rs.getString("user_id");
				int isDeleted = rs.getInt("is_deleted");
				String userName = rs.getString("user_name");


				CommentDao commentDao = new CommentDao();
				List<Comment> comments = commentDao.getComments(connection, id);

				Post post = new Post();
				post.setId(id);
				post.setSubject(subject);
				post.setText(text);
				post.setCategory(category);
				post.setCreatedDate(createdDate);
				post.setUpdatedDate(updatedDate);
				post.setUserId(userId);
				post.setComments(comments);
				post.setUserName(userName);
				post.setIsDeleted(isDeleted);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	public List<Post> getPosts(Connection connection, String word, String start, String end) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT *");
			sql.append(" FROM posts");
			sql.append(" INNER JOIN users");
			sql.append(" ON users.id = posts.user_id");
			sql.append(" WHERE is_deleted = 0");
			sql.append(" AND posts.category");
			sql.append(" LIKE ?");
			sql.append(" AND posts.created_date");
			sql.append(" BETWEEN ?");
			sql.append(" AND ? ");
			sql.append(" ORDER BY posts.created_date DESC");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1,  "%" + word + "%");
			ps.setString(2,  start);
			ps.setString(3,  end);

			ResultSet rs = ps.executeQuery();
			List<Post> ret = toPostList(connection, rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void delete(Connection connection, int postId) {

		PreparedStatement psPost = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE posts SET");
			sql.append(" is_deleted = 1");
			sql.append(" WHERE");
			sql.append(" id = ?");

			psPost = connection.prepareStatement(sql.toString());

			psPost.setInt(1,  postId);
			psPost.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(psPost);
		}
	}

	public Date getPostMinDate(Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT");
			sql.append(" MIN(created_date) AS min_date");
			sql.append(" FROM posts");
			sql.append(" WHERE is_deleted = 0");

			ps = connection.prepareStatement(sql.toString());

			rs = ps.executeQuery();

			Date date = new Date();
			while (rs.next()) {
				Timestamp createdDate = rs.getTimestamp("min_date");
				date = createdDate;
			}
			return date;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(rs);
			close(ps);
		}
	}


	public Date getPostMaxDate(Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT");
			sql.append(" MAX(created_date) AS max_date");
			sql.append(" FROM posts");
			sql.append(" WHERE is_deleted = 0");

			ps = connection.prepareStatement(sql.toString());

			rs = ps.executeQuery();

			Date date = new Date();
			while (rs.next()) {
				Timestamp createdDate = rs.getTimestamp("max_date");
				date = createdDate;
			}
			return date;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(rs);
			close(ps);
		}
	}

	public boolean isPostExist(Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT *");
			sql.append(" FROM posts");
			sql.append(" WHERE is_deleted = 0");

			ps = connection.prepareStatement(sql.toString());

			rs = ps.executeQuery();

			List<Integer> list = new ArrayList<>();
			while (rs.next()) {
				list.add(1);
			}
			if (list.size()==0) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			return false;
		} finally {
			close(rs);
			close(ps);
		}
	}

}
