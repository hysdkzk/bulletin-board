<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="header">
			<div class="profile">ログインユーザー：<c:out value="${loginUser.userName}"/></div>
			<a href="./">ホーム</a>
			<a href="logout">ログアウト</a>
		</div>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			<form action="new_post" method="post">
				新規投稿<br />
				<label for="subject">件名</label><input name="subject" id="subject" /><br />
				<label for="text">本文</label><textarea name="text" id="text" rows="5" /></textarea><br />
				<label for="category">カテゴリー</label><input name="category" id="category" /><br />
				<input type="submit" value="投稿" />
			</form>
		</div>
	</body>
</html>