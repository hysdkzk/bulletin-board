<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<script type="text/javascript">
			function check(){
				if(window.confirm('実行しますか？')){
					return true;
				} else {
					window.alert("キャンセルされました");
					return false;
				}
			}
		</script>
	</head>
	<body>
		<div class="header">
			<div class="profile">ログインユーザー：<c:out value="${loginUser.userName}"/></div>
			<a href="signup">ユーザー新規登録画面</a>
			<a href="./">ホーム画面</a>
			<a href="logout">ログアウト</a>
		</div>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			<br />
			<div class="users">
				<table>
					<tr>
						<th>ID</th>
						<th>名称</th>
						<th>支店コード</th>
						<th>支店名</th>
						<th>部署・役職コード</th>
						<th>部署・役職名</th>
						<th>停止・復活</th>
						<th>編集</th>
					</tr>
					<c:forEach items="${users}" var="user">
						<tr>
							<td><c:out value="${user.id}" /></td>
							<td><c:out value="${user.userName}" /></td>
							<td><c:out value="${user.branchId}" /></td>
							<td><c:out value="${user.branchName}" /></td>
							<td><c:out value="${user.positionId}" /></td>
							<td><c:out value="${user.positionName}" /></td>
							<td>
								<form action="management" method="post" onSubmit="return check()">
									<input name="user_id" value="${user.id}" type="hidden" />
									<input type="submit" name="isStop" value="停止" <c:if test="${ user.isStop == 1 }">disabled="disabled"</c:if> />
									<input type="submit" name="isStop" value="復活" <c:if test="${ user.isStop == 0 }">disabled="disabled"</c:if> />
								</form>
							</td>
							<td>
								<form action="user_edit" method="post">
									<input type="hidden" name="edit_user_id" value="${user.id}" />
									<input type="submit" value="編集" <c:if test="${ user.id == loginUser.id }">disabled="disabled"</c:if> />
								</form>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
		</div>
	</body>
</html>