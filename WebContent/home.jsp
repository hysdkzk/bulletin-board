<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="header">
			<div class="profile">ログインユーザー：<c:out value="${loginUser.userName}"/></div>
			<a href="new_post">新規投稿画面</a>
			<c:if test="${loginUser.positionId == 1 }"><a href="management">ユーザー管理画面</a></c:if>
			<a href="logout">ログアウト</a>
		</div>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			 </c:if>
			<div class="filter">
				<form action="./" method="post">
					<label>カテゴリー（部分一致）</label><input name="category_filter" value="${category}" /><br />
					<label>投稿日（期間指定）</label>
					<input name="start" type="date" value="${start}" /> ～
					<input name="end" type="date" value="${end}" /><br />
					<input type="submit" value="絞込み" id="filter-button" />
				</form>
			</div>
			<div class="posts">
				<c:forEach items="${posts}" var="post">
					<div class="post">
						<div class="post-title">件名：<c:out value="${post.subject}" /></div>
						<div class="post-category">カテゴリー：<c:out value="${post.category}" /></div>
						<div class="post-text"><pre><c:out value="${post.text}" /></pre></div>
						<div class="post-user"><c:out value="${post.userName}" /></div>
						<div class="post-date"><fmt:formatDate value="${post.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
						<div class="delete-post">
							<c:if test="${ loginUser.id == post.userId }">
								<form action="delete_post" method="post">
									<input type="hidden" name="post_id" value="${post.id}" />
									<input type="submit" value="投稿削除" /><br />
								</form>
							</c:if>
						</div>
						<div class="comments">
							<c:forEach items="${post.comments}" var="comment">
								<div class="comment">
									<div class="comment-text"><pre><c:out value="${comment.text}" /></pre></div>
									<div class="comment-user"><c:out value="${comment.userName}" />
									</div>
									<div class="comment-date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
									<c:if test="${ loginUser.id == comment.userId }">
										<div class="delete-comment">
											<form action="delete_comment" method="post">
												<input type="hidden" name="comment_id" value="${comment.id}" />
												<input type="submit" value="コメント削除" /><br />
											</form>
										</div>
									</c:if>
								</div>
							</c:forEach>
							<div class="new-comment">
								<form action="new_comment" method="post">
									<input type="hidden" name="post_id" value="${post.id}" />
									<textarea name="comment" rows="3"></textarea><br />
									<input type="submit" value="コメントする" id="comment-button" />
								</form>
							</div>
						</div>
					</div>
				</c:forEach>
			</div>
		</div>
	</body>
</html>