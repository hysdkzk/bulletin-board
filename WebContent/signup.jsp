<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー新規登録画面</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="header">
			<div class="profile">ログインユーザー：<c:out value="${loginUser.userName}"/></div>
			<a href="management">ユーザー管理画面</a>
			<a href="logout">ログアウト</a>
		</div>
		<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			<form action="signup" method="post">
				ユーザー新規登録<br />
				<label for="id">ログインID</label><input name="id" id="id" /><br />
				<label for="password">パスワード</label><input name="password" type="password" id="password" /><br />
				<label for="password2">パスワード(確認用)</label><input name="password2" type="password" id="password2" /><br />
				<label for="name">名称</label><input name="name" id="name" /><br />
				<label for="branch">支店</label>
				<select name="branch">
					<c:forEach items="${branches}" var="branch">
						<option value="${branch.id}"><c:out value="${branch.id}" />：<c:out value="${branch.name}" /></option>
					</c:forEach>
				</select>
				<label for="position">部署・役職</label>
				<select name="position">
					<c:forEach items="${positions}" var="position">
						<option value="${position.id}"><c:out value="${position.id}" />：<c:out value="${position.name}" /></option>
					</c:forEach>
				</select><br />
				<input type="submit" value="登録" />
			</form>
		</div>
	</body>
</html>